from libqtile.config import Group, Match

class Group(Group):
    def __str__(self):
        return self.name

groups = [
    Group(
        name = "Terminal",
        matches = [
            Match(wm_class="terminal")
        ]
    ),

    Group(
        name = "Browser",
        matches = [
            Match(wm_class="Firefox")
        ]
    )
]
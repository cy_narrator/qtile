from global_config import bar_size, bar_border_width, bar_border_color
from libqtile import bar
from Widgets import widgets
qtile_bar = bar.Bar(
    widgets,
    bar_size,
    border_width = bar_border_width,
    border_color = bar_border_color
)
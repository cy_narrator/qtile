import random
import os

def random_wallpaper(wallpaper_dirs, error_wallpaper, max_tries=2):
    wallpaper_dir = random.choice(wallpaper_dirs)
    supported_wallpaper_format = ['png', 'jpg', 'jpeg', 'gif', 'bmp' 'svg', 'GdkPixdata', 'ico', 'pnm', 'tga', 'tiff' 'ani', 'icns', 'qtif', 'wmf', 'xbm', 'xpm']
    wallpaper_found = False
    while not wallpaper_found:
        wallpaper_dir = os.path.expanduser(wallpaper_dir)
        files_present = os.listdir(wallpaper_dir)
        chosen_wallpaper = random.choice(files_present)
        split_wallpaper = chosen_wallpaper.split(".")
        split_wallpaper_format = split_wallpaper[-1]
        if split_wallpaper_format in supported_wallpaper_format:
            wallpaper_found = True
            return wallpaper_dir + chosen_wallpaper
        else:
            max_tries -= 1
            if max_tries <= 0:
                wallpaper_found = True
                # return f"{os.path.dirname(os.path.realpath(__file__))}/wallpaper_error.jpg"
                return os.path.expanduser(error_wallpaper)
# https://docs.qtile.org/en/latest/manual/config/index.html

from libqtile.utils import guess_terminal
from libqtile import layout
from utils import *

terminal = guess_terminal()

mod = "mod4"

number_of_screens = 1

bar_size = 24
bar_border_width = [2, 0, 2, 0]
bar_border_color = ["ff00ff", "000000", "ff00ff", "000000"]
x11_drag_polling_rate = None


wallpaper_parent_dir = '/usr/share/backgrounds/'
wallpaper_sub_dirs = ['linuxmint-vanessa/', 'linuxmint-virginia/', 'linuxmint/', 'linuxmint-vera/', 'linuxmint-victoria/']
wallpaper_dirs = [wallpaper_parent_dir + wallpaper_sub_dirs[pic] for pic in range(len(wallpaper_sub_dirs))]
wallpaper = random_wallpaper(wallpaper_dirs, '~/.config/qtile/error_wallpaper.jpg')


auto_fullscreen = True

bring_front_click = False

cursor_warp = False

dgroups_key_binder = None

dgroups_app_rules = []

floats_kept_above = True

focus_on_window_activation = 'smart'

follow_mouse_focus = True

reconfigure_screens = False

wmname = 'LG3D'

auto_minimize = True

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()
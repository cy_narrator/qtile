from libqtile import layout
from .float_rules_config import float_rules
floating_layout = layout.Floating(float_rules = float_rules)
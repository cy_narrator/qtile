from .app_launch_keybindings import app_launch_keybindings
from .group_mgmt_keybindings import group_mgmt_keybindings
from .window_mgmt_keybindings import window_mgmt_keybindings
from .qtile_specific_keybindings import qtile_specific_keybindings
from .monitor_mgmt_keybindings import monitor_mgmt_keybindings
from .vt_wayland import vt_keybindings
from libqtile.config import Key

class Key(Key):
    def __str__(self):
        return self.desc

keys = app_launch_keybindings + group_mgmt_keybindings + window_mgmt_keybindings + vt_keybindings + qtile_specific_keybindings + monitor_mgmt_keybindings
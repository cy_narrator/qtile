from libqtile.config import Key
from libqtile.lazy import lazy
from global_config import mod, number_of_screens
monitor_mgmt_keybindings = [
     Key([mod, "control"], str(monitor_id + 1), lazy.to_screen(monitor_id), desc=f"Switch focus to monitor {monitor_id}")
    for monitor_id in range(number_of_screens)
]
from libqtile.config import Key
from libqtile.lazy import lazy
from global_config import mod, terminal

app_launch_keybindings = [
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
]
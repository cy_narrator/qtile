from libqtile.config import Key
from libqtile.lazy import lazy
from libqtile import qtile

vt_keybindings = [
    Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    for vt in range(1, 8)
]
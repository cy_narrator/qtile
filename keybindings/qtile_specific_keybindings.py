from libqtile.config import Key
from libqtile.lazy import lazy
from global_config import mod

qtile_specific_keybindings = [
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]
from libqtile.config import Key
from libqtile.lazy import lazy
from global_config import mod
from groups import groups

# Mod + number switches to the position at which it appears on Groups
# Mod, Shift + Number switches window to that group
group_mgmt_keybindings = []
for group_id in range(len(groups)):
    group = groups[group_id]
    group_mgmt_keybindings.append(Key([mod], str(group_id + 1), lazy.group[group.name].toscreen(), desc=f"Switch to group {group}"))               
    group_mgmt_keybindings.append(Key([mod, "shift"], str(group_id), lazy.window.togroup(group.name, switch_group = True), desc=f"Switch and move window to {group}"))
    
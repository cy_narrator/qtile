from global_config import number_of_screens, x11_drag_polling_rate, wallpaper
from libqtile.config import Screen
from bar import qtile_bar
screens = [
    Screen(
        bottom = qtile_bar,
        x11_drag_polling_rate=x11_drag_polling_rate,
        wallpaper=wallpaper 
    ) for _ in range(number_of_screens)
]